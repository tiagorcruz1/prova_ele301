# -*- coding: utf-8 -*-
"""
Created on Mon May  6 18:30:07 2019

@author: Tiago Cruz
"""

# modulos necessarios
import numpy as np
from math import sqrt
import matplotlib.pyplot as plt

plt.close("all")

# numero de iteracoes
M = 100000

# estimativas
e_R0 = 99.9961
e_A = 0.0039096
e_B = -6e-7
e_Rs = 99.99947
e_r = 1.0780057

# incertezas padrao
u_R0 = 0.0005
u_A = 0.0000027
u_B = 1.1e-7
u_Rs = 0.0001
u_r = 0.000005

# coeficientes de correlacao
r_R0_A = -0.155
r_R0_B = 0.092
r_A_B = -0.959

# matriz de correlação
corr = np.ones((3,3))
corr[0,1] = corr[1,0] = r_R0_A
corr[0,2] = corr[2,0] = r_R0_B
corr[1,2] = corr[2,1] = r_A_B

# cria matriz L por Choesky
L = np.linalg.cholesky(corr)

# distribuicoes de entrada descorrelacionadas
x_0 = np.random.normal(0,1,M) #(mean, stdev, size)
x_1 = np.random.normal(0,1,M)
x_2 = np.random.normal(0,1,M)
Rs = np.random.normal(99.99947, 0.0001, M)
r = np.random.normal(1.0780057, 0.000005,M)

# cria matriz X
x = [x_0, x_1, x_2]

# cria matriz Y com entrdas correlacionadas (Y = LX)
Y = L@x
#Y = x@L
#np.corrcoef(x_corr) # teste 

# ajuste para os valores de interesse
R0 = Y[0]*u_R0+e_R0
A = Y[1]*u_A+e_A
B = Y[2]*u_B+e_B

# saida dada pela equacao R0*B*theta^2 + R0*A*theta +R0-r*Rs = 0
a = R0*B
b = R0*A
c = R0 - r*Rs

theta_1 = []
theta_2 = []

for i in range(0,M):
    theta_1.append((-b[i] + sqrt(b[i]**2 - 4*a[i]*c[i]))/(2*a[i]))
    theta_2.append((-b[i] - sqrt(b[i]**2 - 4*a[i]*c[i]))/(2*a[i]))

# incerteza
corte = int(0.05/2*M)
theta_1 = np.sort(theta_1)
y_95 = theta_1[corte:M-corte]
# incerteza para 95%
U = (y_95[len(y_95)-1] - y_95[0])/2 #ponto final - inicial /2

print("\n-----------------------------------------------")
print("Resultado:")
print("y = ", np.mean(theta_1), "\u00B1", U)
print("-----------------------------------------------")

# histograma de saida 100% de confianca
plt.hist(theta_1, 500)
plt.axvline(x=y_95[0], color = 'black', linewidth = 0.5)
plt.axvline(x=y_95[len(y_95)-1], color = 'black', linewidth = 0.5)
plt.axvline(x=np.mean(theta_1), color = 'black', linewidth = 0.5, linestyle = "--")
plt.xticks((y_95[0], np.mean(theta_1), y_95[len(y_95)-1]), ("-2\u03C3\n"+str(round(y_95[0],4)), round(np.mean(theta_1), 4) , "2\u03C3\n"+str(round(y_95[len(y_95)-1],4))))
plt.arrow(np.mean(theta_1), 50, y_95[len(y_95)-1]-np.mean(theta_1), 0, length_includes_head=True, head_width=10, head_length=0.001, fill=True, color="black")
plt.text((np.mean(theta_1)+y_95[len(y_95)-1])/2, 55, round(U, 4), horizontalalignment='center')