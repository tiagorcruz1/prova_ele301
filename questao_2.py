# -*- coding: utf-8 -*-
"""
Created on Mon May  6 22:53:47 2019

@author: tiago
"""

import numpy as np
import matplotlib.pyplot as plt

plt.close("all")

# numero de amostras
M = 100000

# estimativas
e_vx = 36248
e_delta_t = 0.5 
e_delta_0x = 0

# constantes
cx = 0.026
ctx0 = 0.039

#incertezas de entrada
# observacoes u_vx = a/sqrt(50)
u_1 = 1.6

# laudo de calibracao
u_2 = 1

# resolucao 
u_3 = 1/2

# tensoes parasitas
u_4 = 2

# faixa de temperatura observada
u_5 = 5

# delta t
u_6 = 0.641

# delta tox [°C]
u_7 = 0.1


# distribuicoes de entrada
x1 = np.random.normal(e_vx, u_1, M)
x2 = np.random.uniform(-u_2, u_2, M)
x3 = np.random.uniform(-u_3, u_3, M)
x4 = np.random.uniform(-u_4, u_4, M)
x5 = np.random.uniform(-u_5, u_5, M)
x6 = np.random.uniform(-u_6, u_6, M)
x7 = np.random.uniform(-u_7, u_7, M)

# saida
vx = x1 + x2 + x3 + x4 + x5 + x6/0.026 + x7/0.039

# incerteza
corte = int(0.05/2*M)
vx = np.sort(vx)
y_95 = vx[corte:M-corte]
# incerteza para 95%
U = (y_95[len(y_95)-1] - y_95[0])/2 #ponto final - inicial /2

print("\n-----------------------------------------------")
print("Resultado:")
print("Vx = ", np.mean(vx), "\u00B1", U)
print("-----------------------------------------------")

# histograma de saida 100% de confianca
plt.hist(vx, 500)
plt.axvline(x=y_95[0], color = 'black', linewidth = 0.5)
plt.axvline(x=y_95[len(y_95)-1], color = 'black', linewidth = 0.5)
plt.axvline(x=np.mean(vx), color = 'black', linewidth = 0.5, linestyle = "--")
plt.xticks((y_95[0], np.mean(vx), y_95[len(y_95)-1]), ("-2\u03C3\n"+str(round(y_95[0],3)), round(np.mean(vx), 3) , "2\u03C3\n"+str(round(y_95[len(y_95)-1],3))))
plt.arrow(np.mean(vx), 50, y_95[len(y_95)-1]-np.mean(vx), 0, length_includes_head=True, head_width=10, head_length=1, fill=True, color="black")
plt.text((np.mean(vx)+y_95[len(y_95)-1])/2, 55, round(U, 3), horizontalalignment='center')