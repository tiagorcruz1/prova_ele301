# -*- coding: utf-8 -*-
"""
Created on Mon May  6 22:08:51 2019

@author: Tiago Cruz
"""

import numpy as np
import matplotlib.pyplot as plt

plt.close("all")

# numero de amostras
M = 100000

# coeficientes de correlacao
r_s1_s2 =  0.3
r_s1_s3 =  0.5
r_s2_s3 = -0.3

# matriz de correlacao
corr = np.ones((3,3))
corr[0,1] = corr[1,0] = r_s1_s2
corr[0,2] = corr[2,0] = r_s1_s3
corr[1,2] = corr[2,1] = r_s2_s3

# cria matriz L por Cholesky
L = np.linalg.cholesky(corr)

# distribuicoes descorrelacionada
x1 = np.random.uniform(-1,1,M)
x2 = np.random.uniform(-1,1,M)
x3 = np.random.uniform(-1,1,M)

# cria matriz X
x = [x1, x2, x3]

# cria matriz Y com entrdas correlacionadas (Y = LX)
Y = L@x
#np.corrcoef(Y) # teste 

# estimativas
e_s1 = 5
e_s2 = 10
e_s3 = 15

#incertezas
u_s1 = 0.05
u_s2 = 0.1
u_s3 = 0.15

# ajuste das entradas
s1 = Y[0]*u_s1+e_s1
s2 = Y[1]*u_s2+e_s2
s3 = Y[2]*u_s3+e_s3

# saida
y = s1+s2+s3

# incerteza
corte = int((1-0.6826)/2*M)
y = np.sort(y)
y_1stdev = y[corte:M-corte]
# incerteza para 1 desvio padrao 
U = (y_1stdev[len(y_1stdev)-1] - y_1stdev[0])/2 #ponto final - inicial /2

print("\n-----------------------------------------------")
print("Resultado:")
print("y = ", np.mean(y), "\u00B1", U)
print("-----------------------------------------------")

# histograma de saida 100% de confianca
plt.figure(1)
plt.hist(y, 500)
plt.axvline(x=y_1stdev[0], color = 'black', linewidth = 0.5)
plt.axvline(x=y_1stdev[len(y_1stdev)-1], color = 'black', linewidth = 0.5)
plt.axvline(x=np.mean(y), color = 'black', linewidth = 0.5, linestyle = "--")
plt.xticks((y_1stdev[0], np.mean(y), y_1stdev[len(y_1stdev)-1]), ("-\u03C3\n"+str(round(y_1stdev[0],3)), round(np.mean(y), 3) , "\u03C3\n"+str(round(y_1stdev[len(y_1stdev)-1],3))))
plt.arrow(np.mean(y), 50, y_1stdev[len(y_1stdev)-1]-np.mean(y), 0, length_includes_head=True, head_width=10, head_length=0.01, fill=True, color="black")
plt.text((np.mean(y)+y_1stdev[len(y_1stdev)-1])/2, 55, round(U, 3), horizontalalignment='center')